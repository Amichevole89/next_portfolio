import { Html, Head, Main, NextScript } from "next/document";
import { ThemeProvider } from "next-themes";

export default function Document() {
	return (
		<Html lang='en'>
			<Head />
			<body className='dark:bg-stone-900'>
				<ThemeProvider enableSystem={true} attribute='class'>
					<Main />
				</ThemeProvider>
				<NextScript />
			</body>
		</Html>
	);
}
