"use client"; // this is a client component
import React from "react";
import Image from "next/image";
import { Link } from "react-scroll/modules";
import { HiArrowDown } from "react-icons/hi";
import { scrollOptions } from "../../ListProjects";
const HeroSection = () => {
	return (
		<section id='home'>
			<div className='flex flex-col text-center items-center justify-center animate-fadeIn animation-delay-2 my-10 py-16 sm:py-32 md:py-48 md:flex-row md:space-x-4 md:text-left'>
				<div className='md:mt-2 md:w-1/2'>
					<Image
						src='/headshot.png'
						alt=''
						width={325}
						height={325}
						className='rounded-full shadow-2xl'
						loading='lazy' // Enable lazy loading
					/>
				</div>
				<div className='md:mt-2 md:w-3/5'>
					<h1 className='text-4xl font-bold mt-6 md:mt-0 md:text-7xl'>Hi, I&#39;m Tyler!</h1>
					<p className='text-lg mt-4 mb-6 md:text-2xl'>
						I&#39;m a <span className='font-semibold text-teal-600'>Full-Stack Software Engineer </span>
						based in Nazareth, PA. Working towards creating impactful software that makes life easier and more
						meaningful.
					</p>
					<Link
						to='projects'
						className='inline-block py-3 px-6 text-neutral-100 font-semibold bg-teal-600 rounded shadow hover:translate-y-2 hover:translate-x-2 hover:shadow-md dark:hover:bg-white dark:hover:text-teal-600 hover:bg-neutral-700 transition-all duration-300'
						{...scrollOptions} // Use scroll options constant
					>
						Projects
					</Link>
				</div>
			</div>
			<div className='flex flex-row items-center text-center justify-center '>
				<Link to='about' {...scrollOptions}>
					{/* Use scroll options constant */}
					<HiArrowDown size={35} className='animate-bounce' />
				</Link>
			</div>
		</section>
	);
};

export default HeroSection;
