import React from "react";
import SlideUp from "./SlideUp";
import { Link } from "react-scroll/modules";
import { HiArrowDown } from "react-icons/hi";
const scrollOptions = {
	activeClass: "active",
	spy: true,
	smooth: true,
	offset: -100,
	duration: 500,
};
const AboutSection = () => {
	return (
		<section id='about'>
			<SlideUp offset='-300px 0px -300px 0px'>
				<div className='my-12 pb-12 md:pt-16 md:pb-48'>
					<h1 className='text-center font-bold text-4xl'>
						About Me
						<hr className='w-6 h-1 mx-auto my-4 bg-teal-500 border-0 rounded' />
					</h1>

					<div className='flex flex-col space-y-10 items-stretch justify-center align-top md:space-x-10 md:space-y-0 md:p-4 md:flex-row md:text-left'>
						<div className='md:w-6/8'>
							<h1 className='text-center text-2xl font-bold mb-6 md:text-left'>Get to know me!</h1>
							<p className='mb-4'>
								Hi, my name is Tyler and I am a <span className='font-bold'>highly ambitious</span>,{" "}
								<span className='font-bold'>self-motivated</span>, and <span className='font-bold'>driven</span>{" "}
								Software Engineer based in Nazareth, PA.
							</p>
							<p className='mb-4'>
								I graduated from Hack Reactor in 2022 as a Certified Full-Stack Software Engineer specializing in
								Python and JavaScript.
							</p>
							<p className='mb-4'>
								I have a wide range of hobbies and passions that keep me busy. From golfing, collecting vinyl records,
								gardening, to welding, I am always seeking new experiences and love to keep myself engaged and learning
								new things.
							</p>
							<p>
								I believe that you should <span className='font-bold text-teal-500'>never stop growing</span>
							</p>
							<br />
							<p>
								and that&#39;s what I strive to do, I have a passion for technology and a desire to always push the
								limits of what is possible. I am excited to see where my career takes me and am always open to new
								opportunities.🙂
							</p>
						</div>
					</div>
				</div>
			</SlideUp>
			<div className='flex flex-row items-center text-center justify-center '>
				<Link to='skills' {...scrollOptions}>
					{/* Use scroll options constant */}
					<HiArrowDown size={35} className='animate-bounce' />
				</Link>
			</div>
		</section>
	);
};
export default AboutSection;
