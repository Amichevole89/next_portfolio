import React from "react";
import SlideUp from "./SlideUp";
import { Link } from "react-scroll/modules";
import { HiArrowDown } from "react-icons/hi";
const skills = [
	{ skill: "JavaScript" },
	{ skill: "TypeScript" },
	{ skill: "Python" },
	{ skill: "Docker" },
	{ skill: "FastAPI" },
	{ skill: "Starlette" },
	{ skill: "RESTful APIs" },
	{ skill: "Django" },
	{ skill: "React.js" },
	{ skill: "Node.js" },
	{ skill: "Redux" },
	{ skill: "RabbitMQ" },
	{ skill: "Next.js" },
	{ skill: "HTML5" },
	{ skill: "CSS3" },
	{ skill: "Material-UI" },
	{ skill: "Bootstrap" },
	{ skill: "Tailwind CSS" },
	{ skill: "Git" },
	{ skill: "GitLab" },
	{ skill: "MongoDB" },
	{ skill: "PostgreSQL" },
	{ skill: "Agile Methodologies" },
	{ skill: "Test-Driven Development" },
	{ skill: "Domain-Driven Design" },
	{ skill: "Object-Oriented Programming" },
];
const scrollOptions = {
	activeClass: "active",
	spy: true,
	smooth: true,
	offset: -100,
	duration: 500,
};
const SkillsSection = () => {
	return (
		<section id='skills'>
			<SlideUp offset='-300px 0px -300px 0px'>
				<div className='my-6 pb-12 md:pt-16 md:pb-48'>
					<h1 className='text-center font-bold text-4xl'>
						Skills
						<hr className='w-6 h-1 mx-auto my-4 bg-teal-500 border-0 rounded' />
					</h1>

					<div className='flex flex-col space-y-10 items-stretch justify-center align-top md:space-x-10 md:space-y-0 md:p-4 md:flex-row md:text-left'>
						<div className='text-center md:w-6/8 md:text-left'>
							<h1 className='text-2xl font-bold mb-6'></h1>
							<div className='flex flex-wrap flex-row justify-start z-10 md:justify-start'>
								{skills.map((item, idx) => (
									<p key={idx} className='bg-gray-200 px-4 py-2 mr-2 mt-2 text-gray-500 rounded font-semibold'>
										{item.skill}
									</p>
								))}
							</div>
						</div>
					</div>
				</div>
				<div className='flex flex-row items-center text-center justify-center '>
					<Link to='projects' {...scrollOptions}>
						{/* Use scroll options constant */}
						<HiArrowDown size={35} className='animate-bounce' />
					</Link>
				</div>
			</SlideUp>
		</section>
	);
};
export default SkillsSection;
