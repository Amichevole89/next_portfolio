import React from "react";
import { AiOutlineGitlab, AiOutlineLinkedin } from "react-icons/ai";
import Link from "next/link";

const Footer = () => {
	const socialLinks = [
		{ href: "https://gitlab.com/Amichevole89", icon: <AiOutlineGitlab size={30} /> },
		{ href: "https://www.linkedin.com/in/t-male/", icon: <AiOutlineLinkedin size={30} /> },
	];

	return (
		<footer className='mx-auto max-w-3xl px-4 sm:px-6 md:max-w-5xl '>
			<hr className='w-full h-0.5 mx-auto mt-8 bg-neutral-200 border-0'></hr>
			<div className='mx-auto p-4 flex flex-col text-center text-neutral-900 md:flex-row md:justify-between'>
				<div className='flex flex-row items-center justify-center space-x-1 text-neutral-500 dark:text-neutral-100'>
					© 2023 Tyler Male<Link href='/' className='hover:underline'></Link>
				</div>
				<div className='flex flex-row items-center justify-center space-x-2 mb-1'>
					{socialLinks.map(({ href, icon }) => (
						<Link href={href} key={href}>
							{icon}
						</Link>
					))}
				</div>
			</div>
		</footer>
	);
};

export default Footer;
