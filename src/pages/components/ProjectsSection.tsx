import React from "react";
import Image from "next/image";
import Link from "next/link";
import { BsArrowUpRightSquare } from "react-icons/bs";
import { FaGitlab } from "react-icons/fa";
import SlideUp from "./SlideUp";
import { Projects, projects } from "../../ListProjects";

const Project = ({ name, description, image, github, link }: Projects) => {
	return (
		<SlideUp offset='-300px 0px -300px 0px'>
			<div className='flex flex-col animate-slideUpCubiBezier animation-delay-2 md:flex-row md:space-x-12'>
				<div className=' md:w-1/2'>
					<Link href={link}>
						<Image
							src={image}
							alt={name}
							width={1000}
							height={1000}
							className='rounded-xl shadow-xl hover:opacity-70'
						/>
					</Link>
				</div>
				<div className='mt-8 md:w-1/2'>
					<h1 className='text-4xl font-bold mb-6'>{name}</h1>
					<p className='text-xl leading-7 mb-4 text-neutral-600 dark:text-neutral-400'>{description}</p>
					<div className='flex flex-row align-bottom space-x-4'>
						<Link href={github} target='_blank' rel='noopener noreferrer'>
							<FaGitlab size={30} className='hover:-translate-y-1 transition-transform cursor-pointer' />
						</Link>
						<Link href={link} target='_blank' rel='noopener noreferrer'>
							<BsArrowUpRightSquare size={30} className='hover:-translate-y-1 transition-transform cursor-pointer' />
						</Link>
					</div>
				</div>
			</div>
		</SlideUp>
	);
};

const ProjectsSection = () => {
	return (
		<section id='projects'>
			<h1 className='my-10 text-center font-bold text-4xl'>
				Projects
				<hr className='w-6 h-1 mx-auto my-4 bg-teal-500 border-0 rounded' />
			</h1>

			<div className='flex flex-col space-y-28'>
				{projects.map(({ name, description, image, github, link }, idx) => {
					return (
						<div key={idx}>
							<Project key={idx} name={name} description={description} image={image} github={github} link={link} />
						</div>
					);
				})}
			</div>
		</section>
	);
};

export default ProjectsSection;
