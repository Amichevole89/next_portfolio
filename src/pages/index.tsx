import Head from "next/head";
// import Image from "next/image";
import { Inter } from "next/font/google";
import SkillsSection from "./components/SkillsSection";
import AboutSection from "./components/AboutSection";
import MainSection from "./components/PhotoSection";
import ProjectsSection from "./components/ProjectsSection";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
// const inter = Inter({ subsets: ["latin"] });

export default function Home() {
	return (
		<>
			<Head>
				<title>Tyler Male</title>
				<meta content='width=device-width, initial-scale=1' name='viewport' />
				<meta name='description' content='Generated by create next app' />
				<link rel='icon' href='https://tinyurl.com/bdsaexnc' />
			</Head>
			<Navbar />
			<main className='mx-auto max-w-3xl px-4 sm:px-6 md:max-w'>
				<MainSection />
				<AboutSection />
				<SkillsSection />
				<ProjectsSection />
			</main>
			<Footer />
		</>
	);
}
