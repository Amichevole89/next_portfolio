export type Projects = { name: string; description: string; image: string; github: string; link: string };

export const projects: Projects[] = [
	{
		name: "Friends Fur Real",
		description:
			"Friends Fur Real is mobile friendly, scalable platform for all animal rescues to list pets for adoption.  This application was built using the FARM stack in 1 month by a team of 4 developers",
		image: "/ffr_preview.gif",
		github: "https://gitlab.com/friends-dep/friends-deploy",
		link: "https://friends-fur-real.onrender.com",
	},
	{
		name: "Dealership Solutions",
		description: "A platform for car dealerships to manage inventory, sales, and services.",
		image: "/dealership-preview.gif",
		github: "https://gitlab.com/Amichevole89/dealership-solutions",
		link: "https://dealership-solutions.onrender.com",
	},
	{
		name: "Conference Go",
		description:
			"A platform for users to search for and attend conferences.  This uses two third-party APIs, one to integrate weather data for conferences and another to add photos based on location.",
		image: "/dealership-preview.gif",
		github: "https://gitlab.com/Amichevole89/conference-go",
		link: "https://gitlab.com/Amichevole89/conference-go",
	},
	{
		name: "Scrumptious Recipes",
		description:
			"A django platform to create recipes, make mealplans, and add items to your shopping cart with CRUD for each. This project also has a second project integrated into it, called Alpha, which allows users to create and manage projects and tasks. .",
		image: "/Scrumptious.png",
		github: "https://gitlab.com/Amichevole89/alpha-scrumptious",
		link: "https://gitlab.com/Amichevole89/alpha-scrumptious",
	},
];

export type ScrollOption = { activeClass: string; spy: boolean; smooth: boolean; offset: number; duration: number };

export const scrollOptions: ScrollOption = {
	activeClass: "active",
	spy: true,
	smooth: true,
	offset: -100,
	duration: 500,
};
